import pyautogui as pag
import time
import pyperclip

# Define the coordinates and use the `actions` list
actions = [
    (439, 367, 2),  # klik kanan
    (509, 497, 2),  # select
    (460, 366, 2),  # klik kanan
    (512, 429, 2),  # COPYID
]

# Wait for a few seconds to give time to focus on the target application
time.sleep(5)
# Perform the actions in the specified order
for x, y, duration in actions:
    if (x, y) == (439, 367) or (x, y) == (460, 366):
        # For right-click coordinates, perform right-click
        pag.rightClick(x, y, duration=duration)
    else:
        pag.click(x, y, duration=duration)
    if (x, y) in [(560, 304)]:
        # For "first fill" and "second fill" coordinates, type the desired text
        pag.keyDown('r')  # Press the "D" key
        text_to_type = "uuiii"
        pag.typewrite(text_to_type)
    if (x, y) in [(564, 366)]:
        pag.keyDown('R')
        text_to_type = "ooiu"
        pag.typewrite(text_to_type)
    if (x, y) in [(336, 436)]:
        pag.keyDown('D')
        text_to_type = "khafidzu"
        pag.typewrite(text_to_type)

def save_echo_to_batch(file_path, echo_text):
    with open(file_path, 'a') as file:
        file.write(f'\necho {echo_text}\n')

def run_rustdesk_command():
    clipboard_text = pyperclip.paste()
    password_echo = 'Password : Dkhafidzu'  
    save_echo_to_batch('show.bat', f'RustDesk ID: {clipboard_text}')
    save_echo_to_batch('show.bat', password_echo)


if __name__ == "__main__":
    run_rustdesk_command()

print("Done , Log in Credintials is below")
