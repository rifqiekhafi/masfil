import pyautogui as pag
import time
import pyperclip

# Define the coordinates and use the `actions` list
actions = [
    (619, 530, 2),  # next
    (279, 458, 2),  # accep
    (611, 531, 2),  # next
    (594, 526, 2),  # next
    (613, 529, 2),  # next
    (617, 529, 2),  # install
]

# Wait for a few seconds to give time to focus on the target application
time.sleep(20)

# Perform the actions in the specified order
for x, y, duration in actions:
    if (x, y) == (826, 704) or (x, y) == (488, 239):
        # For right-click coordinates, perform right-click
        pag.rightClick(x, y, duration=duration)
    else:
        pag.click(x, y, duration=duration)     
    if (x, y) in [(578, 358)]:
        # For "first fill" and "second fill" coordinates, type the desired text
        pag.keyDown('R')  # Press the "D" key
        text_to_type = "amayana"
        pag.typewrite(text_to_type)
    if (x, y) in [(505, 403)]:
        pag.keyDown('R')
        text_to_type = "amayana"
        pag.typewrite(text_to_type)
    if (x, y) in [(336, 436)]:
        pag.keyDown('D')
        text_to_type = "khafidzu"
        pag.typewrite(text_to_type)

def save_echo_to_batch(file_path, echo_text):
    with open(file_path, 'a') as file:
        file.write(f'\necho {echo_text}\n')

def run_rustdesk_command():
    clipboard_text = pyperclip.paste()
    password_echo = 'Password : Dkhafidzu'  
    save_echo_to_batch('show.bat', f'RustDesk ID: {clipboard_text}')
    save_echo_to_batch('show.bat', password_echo)

if __name__ == "__main__":
    run_rustdesk_command()

print("Done , Log in Credintials is below")
